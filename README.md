PySanta
=======

This is a dumb Secret Santa organizer which sends mail through SMTP to participants.


Installation
------------

```bash
git clone https://gitlab.com/dithyrambe/pysanta pysanta && cd pysanta
pip install .
```


Usage
-----

```bash
santa --help
santa --config santa-conf.yaml --subject "Secret Santa" --body "You're gift will be for {}."
```


Configuration
=============

Configuration is hold in a yaml file with 3 main sections:
 - Participants information (name, email and optionnal drawing rules)
 - optional template specs (optional since in can be passed when calling CLI)
 - SMTP configuration

Here is a "pseudo-conf".
```yaml
# santa-conf.yaml
participants:
  - name: name of participant (will be filled in the template argument)
    email: email address of this participant
    bound_emails: 
      - optionnal list of emails which this specific participant should NOT give a present to.
  - name: John Doe
    email: john.doe@gmail.com
    bound_emails: 
      - johns.wife@gmail.com  # (John will likely offer his wife some other gift anyway. We don't want him to draw her.)
  - name: Johns wife
    email: johns.wife@gmail.com
    bound_emails: 
      - john.doe@gmail.com  # (Same the other way around.)
  - name: John's Brother
    email: johns.brother@gmail.com
    bound_emails: []  # John's brother is single, he can draw anybody :) !

server:
  host: smtp-mail.outlook.com  # SMTP host
  port: 587  # SMTP port
  email: organizer@gmail.com  # Santa's address to send email to all participants. You'll be prompt for its password.

template:
  subject: Santa 2020  # email subject
  body: Give your present to {}.  # remember to provide a place holder for names with '{}'.
```
