import os
from setuptools import setup, find_packages

HERE = os.path.dirname(os.path.abspath(__file__))


with open(os.path.join(HERE, "README.md"), encoding="utf8") as _f:
    readme = _f.read()

with open(os.path.join(HERE, "requirements.txt"), encoding="utf8") as _f:
    reqs = _f.read().split()

setup(
    name="pysanta",
    packages=find_packages(),
    description="Secret Santa",
    long_description=readme,
    long_description_content_type="text/markdown",
    install_requires=reqs,
    url="https://gitlab.com/dithyrambe/pysanta",
    classifiers=[
        "Programming Language :: Python :: 3.8",
    ],
    license="MIT",
    entry_points={
        'console_scripts': [
            "santa = pysanta.cli:santa"
        ]
    }
)