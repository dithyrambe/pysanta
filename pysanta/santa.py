from dataclasses import dataclass, field
import numpy as np
import yaml


@dataclass
class Participant:
    name: str
    email: str
    bounded_emails: list = field(default_factory=lambda: [])

    def __hash__(self):
        return hash(self.email)


def read_config(path):
    with open(path) as f:
        conf = yaml.load(f, Loader=yaml.FullLoader)
    participants = [Participant(**kwargs) for kwargs in conf["participants"]]
    server_conf = conf["server"]
    template_conf = conf.get("template", {})
    return participants, server_conf, template_conf


def cycle_window(array, size=2):
    length = len(array)
    base = np.arange(size)
    for i, _ in enumerate(array):
        indices = (base + i) % length
        yield indices, array[indices]


def check_rule(rule):
    """Check for inconsistencies in final rule."""
    for giver, receiver in rule.items():
        if giver.email == receiver.email:
            raise ValueError(f"{giver.email} will have to offer himself.")
        if receiver.email in giver.bounded_emails:
            raise ValueError(f"Cannot offer to a bounded email.")


def adjust_rule(shuffled):
    for (i, j, k), (giver, receiver, alt) in cycle_window(shuffled, size=3):
        if receiver.email in giver.bounded_emails:
            shuffled[j], shuffled[k] = shuffled[k], shuffled[j]
    return shuffled


def make_rule(participants):
    participants = np.array([*participants])
    indices = np.arange(len(participants))
    np.random.shuffle(indices)
    shuffled = participants[indices]
    adjusted = adjust_rule(shuffled)
    return {giver: receiver for _, (giver, receiver) in cycle_window(adjusted, size=2)}
