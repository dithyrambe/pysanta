from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
import smtplib


class MailClient:
    def __init__(self, conf, password):
        self.conf = conf
        self.server = smtplib.SMTP(
            host=self.conf["host"],
            port=self.conf["port"]
        )
        self.server.starttls()
        self.server.login(self.conf["email"], password)

    def send_msg(self, to, subject, body):
        msg = MIMEMultipart()
        msg["From"] = self.conf["email"]
        msg["To"] = to
        msg["Subject"] = subject
        msg.attach(MIMEText(body, "plain"))
        self.server.send_message(msg)
