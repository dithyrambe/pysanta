import click

from .santa import read_config, make_rule, check_rule
from .smtp import MailClient


@click.command()
@click.option("-c", "--config", required=True, type=str, help="Path to config.")
@click.option("-s", "--subject", default=None, type=str, help="Email subject.")
@click.option(
    "-b", "--body", default=None, type=str,
    help="Email template. Placeholder {} will be filled with participant names."
)
@click.option(
    "--dry-run", is_flag=True, help="Only print the rule to stdout without sending emails"
)
def santa(config, subject, body, dry_run):
    participants, server_conf, template_conf = read_config(config)
    rule = make_rule(participants)
    check_rule(rule)

    password = click.prompt(f"Password for {server_conf['email']}", hide_input=True)
    mail_client = MailClient(server_conf, password)

    if dry_run:
        click.echo("\n".join(
            (
                f"TO: {giver.email}: "
                f"SUBJECT: {subject or template_conf['subject']} "
                f"BODY: {(body or template_conf['body']).format(receiver.name)}"
                for giver, receiver in rule.items()
            )
        ))
    else:
        for giver, receiver in rule:
            mail_client.send_msg(
                to=giver.email,
                subject=subject or template_conf['subject'],
                body=(body or template_conf['body']).format(receiver.name)
            )


if __name__ == "__main__":
    santa()
